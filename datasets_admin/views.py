# coding: utf-8

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.conf import settings

from pysoda import *

from pn_forests_portal.groups import allowed_groups
from datasets.settings import STORAGE_BACKEND, DATA_PATH
from datasets_admin.forms import CsvFolderForm, SQLDatastoreForm

DATASTORES = {
    CsvFolderDatastore: ("csv_folder", "Dossier CSV"),
    SQLDatastore: ("pg_db", "Base PostgreSQL")
}
DATASTORES_REV = {
    "csv_folder": CsvFolderDatastore,
    "pg_db": SQLDatastore,
}


@allowed_groups("datasets_admin")
@login_required(login_url=settings.LOGIN_URL)
def datasets_admin_index(request):
    return render(request, 'datasets_admin/datasets_admin.html',
                  {'storage_backend': STORAGE_BACKEND}, )


@allowed_groups("datasets_admin")
@login_required(login_url=settings.LOGIN_URL)
def add_datastore(request):
    supported_datastores = [
        DATASTORES[CsvFolderDatastore],
        DATASTORES[SQLDatastore],
    ]
    selected_datastore = {
        'csv_folder': False,
        'pg_db': False,
    }
    csv_folder_form = CsvFolderForm()
    pg_db_form = SQLDatastoreForm()
    if request.POST:
        datastore_type = request.POST['datastore_type']
        csv_folder_form = CsvFolderForm(request.POST)
        pg_db_form = SQLDatastoreForm(request.POST)
        datastore = None
        if datastore_type == 'csv_folder':
            if csv_folder_form.is_valid():
                datastore = CsvFolderDatastore(
                    csv_folder_form.data['csv_folder_name'],
                    DATA_PATH,
                    csv_folder_form.data['init_csv_folder'],
                )
            selected_datastore['csv_folder'] = True
        elif datastore_type == 'pg_db':
            if pg_db_form.is_valid():
                url = "postgresql://{}:{}@{}/{}".format(
                    pg_db_form.data['user_name'],
                    pg_db_form.data['password'],
                    pg_db_form.data['host'],
                    pg_db_form.data['database_name'],
                )
                datastore = SQLDatastore(
                    pg_db_form.data['database_name'],
                    url,
                    pg_db_form.data['init_datasets_meta'],
                )
            selected_datastore['pg_db'] = True
        if datastore is not None:
            STORAGE_BACKEND.register_datastore(datastore)
            return redirect('/datasets_admin/')
    sup_dat = [(d[0], d[1], selected_datastore[d[0]])
               for d in supported_datastores]
    return render(request, 'datasets_admin/add_datastore.html',
                  {'supported_datastores': sup_dat,
                   'csv_folder_form': csv_folder_form,
                   'pg_db_form': pg_db_form,})
