$(document).ready(function() {
    var datastore_type = $("input[type=radio][name=datastore_type]");
    var csv_folder = $("#csv_folder");
    var pg_db = $("#pg_db");

    function check_datastore_type() {
        val = this.value;
        if (!this.checked) {
            return;
        }
        if (val == "csv_folder") {
            csv_folder.removeClass("hidden");
            pg_db.addClass("hidden");
        } else if (val == "pg_db") {
            csv_folder.addClass("hidden");
            pg_db.removeClass("hidden");
        } else {
            csv_folder.addClass("hidden");
            pg_db.addClass("hidden");
        }
    };

    datastore_type.change(check_datastore_type);

    datastore_type.change();
});
