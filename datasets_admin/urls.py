# coding: utf-8

from django.conf.urls import url

from datasets_admin import views


urlpatterns = [
    url(r'^$', views.datasets_admin_index),
    url(r'^add_datastore$', views.add_datastore),
]
