# coding: utf-8

from django import forms
from django.forms import RadioSelect


class CsvFolderForm(forms.Form):

    csv_folder_name = forms.CharField(
        label="Nom du magasin de données (nom du dossier CSV)",
    )
    init_csv_folder = forms.TypedChoiceField(
        label="Initialiser (créer le dossier CSV)",
        coerce=lambda x: x == 'Oui',
        choices=((True, 'Oui'), (False, 'Non')),
        widget=RadioSelect(),
    )


class SQLDatastoreForm(forms.Form):

    database_name = forms.CharField(
        label="Nom du magasin de données (nom de la base PostgreSQL)",
    )
    init_datasets_meta = forms.TypedChoiceField(
        label="Initialiser (créer la table 'datasets_meta')",
        coerce=lambda x: x == 'Oui',
        choices=((True, 'Oui'), (False, 'Non')),
        widget=RadioSelect(),
    )
    user_name = forms.CharField(
        label="Nom de l'utilisateur PostgreSQL",
    )
    password = forms.CharField(
        label="Mot de passe de l'utilisateur PostgreSQL",
        widget=forms.PasswordInput,
    )
    host = forms.CharField(
        label="Hôte de la base de données",
    )
