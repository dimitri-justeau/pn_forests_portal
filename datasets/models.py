# coding: utf-8

from django.db import connections

from pysoda.src.storage_backends.django_storage_backend.models \
    import DjangoStorageBackendModel


def create_storage_backend_model():
    with connections['default'].schema_editor() as schema_editor:
        schema_editor.create_model(DjangoStorageBackendModel)

