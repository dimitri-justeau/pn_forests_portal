# coding: utf-8

from pysoda import *


JSON_STORAGE_PATH = 'storage.json'

DATA_PATH = "data"

JSON_STORAGE_BACKEND = make_json_storage_backend(
    JSON_STORAGE_PATH,
    init=True,
    overwrite=False
)

STORAGE_BACKEND = make_django_storage_backend()
