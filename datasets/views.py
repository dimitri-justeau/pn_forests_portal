# coding: utf-8

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.conf import settings

from pysoda.src.dataset_services import *

from datasets.settings import STORAGE_BACKEND
from pn_forests_portal.groups import allowed_groups


SERVICES = {
    MockService: [('mock', 'mock!'), ],
    TableReadService: [
        ('table_read', 'Tableau de données'),
        ('export', 'Export des données', ['csv', ]),
    ],
    GeoTableService: [
        ('table_read', 'Tableau de données'),
        ('export', 'Export des données', ['csv', 'geojson']),
    ],
    TableWriteService: [('table_write', 'Editer le tableau de données'), ]
}


@allowed_groups("datasets")
@login_required(login_url=settings.LOGIN_URL)
def storage_index(request):
    return render(request, 'datasets/storage.html',
                  {'storage_backend': STORAGE_BACKEND})


@allowed_groups("datasets")
@login_required(login_url=settings.LOGIN_URL)
def datastore_index(request, datastore_name):
    datastore = STORAGE_BACKEND.get_datastore(datastore_name)
    return render(request, 'datasets/datastore.html',
                  {'datastore': datastore})


@allowed_groups("datasets")
@login_required(login_url=settings.LOGIN_URL)
def dataset_index(request, datastore_name, dataset_name):
    datastore = STORAGE_BACKEND.get_datastore(datastore_name)
    dataset = datastore.get_dataset(dataset_name)
    services = [SERVICES[s] for s in dataset.provided_services()]
    return render(request, 'datasets/dataset.html',
                  {'dataset': dataset,
                   'services': services, })
