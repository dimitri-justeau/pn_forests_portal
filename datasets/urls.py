# coding: utf-8

from django.conf.urls import url, include

from datasets import views

urlpatterns = [
    url(r'^$', views.storage_index),
    url(r'^([a-z_0-9\w ]*)/$', views.datastore_index),
    url(r'^([a-z_0-9\w ]*)/([a-z_1-9\w ]*)/$', views.dataset_index),
    url(r'^', include('dataset_services.table_read.urls')),
]
