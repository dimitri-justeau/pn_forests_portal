# coding: utf-8

from django.contrib import admin
from datasets.models import DjangoStorageBackendModel


class DjangoStorageBackendModelAdmin(admin.ModelAdmin):
    pass


admin.site.register(DjangoStorageBackendModel, DjangoStorageBackendModelAdmin)
