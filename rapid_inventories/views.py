# coding: utf-8

from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.gis.geos.point import Point
from django.contrib.auth.decorators import login_required
from django.conf import settings
from djgeojson.serializers import Serializer as GeoSerializer

from rapid_inventories.forms import RapidInventoryForm,\
    GeneralInformationsForm,\
    MeasuresFromCenterForm,\
    VegetationDescriptionForm,\
    MeasuresWalkingForm
from rapid_inventories.models import RapidInventory


@login_required(login_url=settings.LOGIN_URL)
def rapid_inventories_index(request):
    fields = [
        'inventory_date',
        'observer_full_name',
        'location_description',
        'location',
        'consult',
    ]
    header = [
        "Date de l'inventaire",
        "Observateur",
        "Localisation (description)",
        "Localisation (longitude/latitude WGS84)",
        "",
    ]
    inventories = RapidInventory.objects.order_by('inventory_date')\
        .select_related('observer')

    def get_val(inv, f):
        if f == 'location':
            return getattr(inv, f).x, getattr(inv, f).y
        elif f == 'consult':
            return '<a href="{}">consulter</a>'.format(inv.id)
        return getattr(inv, f)

    data = [[get_val(inv, f) for f in fields] for inv in inventories]
    return render(request, 'rapid_inventories/consult_inventories.html',
                  {'inventories': data, 'header': header, })


@login_required(login_url=settings.LOGIN_URL)
def add_rapid_inventory(request):
    long = ''
    lat = ''
    error_location = False
    if request.POST:
        form = RapidInventoryForm(request.POST)
        general_form = GeneralInformationsForm(request.POST)
        center_form = MeasuresFromCenterForm(request.POST)
        vegetation_form = VegetationDescriptionForm(request.POST)
        walking_form = MeasuresWalkingForm(request.POST)
        long = request.POST['long']
        lat = request.POST['lat']
        error_location = not (len(long) > 0 and len(lat) > 0)
        is_valid = general_form.is_valid() and center_form.is_valid()\
            and vegetation_form.is_valid() and walking_form.is_valid()\
            and len(long) > 0 and len(lat) > 0
        if is_valid:
            location = Point(float(long), float(lat))
            inventory = form.save(commit=False)
            inventory.location = location
            inventory.observer = request.user
            inventory.save()
            return redirect('/rapid_inventories/')
    else:
        form = RapidInventoryForm()
        general_form = GeneralInformationsForm()
        center_form = MeasuresFromCenterForm()
        vegetation_form = VegetationDescriptionForm()
        walking_form = MeasuresWalkingForm()
    return render(request, 'rapid_inventories/add_inventory.html', {
        'form': form,
        'general_form': general_form,
        'center_form': center_form,
        'vegetation_form': vegetation_form,
        'walking_form': walking_form,
        'long': long,
        'lat': lat,
        'error_location': error_location,
    })


@login_required(login_url=settings.LOGIN_URL)
def consult_rapid_inventory(request, inventory_id):
    inventory = RapidInventory.objects.get(id=inventory_id)
    long, lat = [str(i) for i in inventory.location.coords]
    error_location = False
    read_only = request.user != inventory.observer
    if request.POST:
        form = RapidInventoryForm(request.POST, instance=inventory)
        general_form = GeneralInformationsForm(request.POST)
        center_form = MeasuresFromCenterForm(request.POST)
        vegetation_form = VegetationDescriptionForm(request.POST)
        walking_form = MeasuresWalkingForm(request.POST)
        long = request.POST['long']
        lat = request.POST['lat']
        error_location = not (len(long) > 0 and len(lat) > 0)
        is_valid = general_form.is_valid() and center_form.is_valid()\
            and vegetation_form.is_valid() and walking_form.is_valid()\
            and len(long) > 0 and len(lat) > 0
        if is_valid:
            location = Point(float(long), float(lat))
            inventory = form.save(commit=False)
            inventory.location = location
            inventory.observer = request.user
            inventory.save()
            return redirect('/rapid_inventories/')
    else:
        kwarg = {'instance': inventory, 'read_only': read_only}
        form = RapidInventoryForm(**kwarg)
        general_form = GeneralInformationsForm(**kwarg)
        center_form = MeasuresFromCenterForm(**kwarg)
        vegetation_form = VegetationDescriptionForm(**kwarg)
        walking_form = MeasuresWalkingForm(**kwarg)
    return render(request, 'rapid_inventories/inventory.html', {
        'form': form,
        'general_form': general_form,
        'center_form': center_form,
        'vegetation_form': vegetation_form,
        'walking_form': walking_form,
        'long': long,
        'lat': lat,
        'error_location': error_location,
        'read_only': read_only,
    })


@login_required(login_url=settings.LOGIN_URL)
def delete_rapid_inventory(request, inventory_id):
    inventory = RapidInventory.objects.get(id=inventory_id)
    inventory.delete()
    return redirect('/rapid_inventories/')


@login_required(login_url=settings.LOGIN_URL)
def rapid_inventory_data(request):
    massifs = RapidInventory.objects.select_related('observer')
    properties = {
        'id': 'id',
        'inventory_date': 'inventory_date',
        'location_description': 'location_description',
        'observer_full_name': 'observer',
    }
    inventories_geojson = GeoSerializer().serialize(
        massifs,
        srid=4326,
        geometry_field='location',
        properties=properties,
    )
    return HttpResponse(inventories_geojson)
