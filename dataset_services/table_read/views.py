# coding: utf-8

from django.http.response import HttpResponse
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings

from datasets.settings import STORAGE_BACKEND
from pn_forests_portal.groups import allowed_groups


@allowed_groups("datasets")
@login_required(login_url=settings.LOGIN_URL)
def table_read(request, datastore_name, dataset_name):
    datastore = STORAGE_BACKEND.get_datastore(datastore_name)
    dataset = datastore.get_dataset(dataset_name)
    header = dataset.get_header_names()
    row_list = dataset.get_rows()
    paginator = Paginator(row_list, 25)
    page = request.GET.get('page')
    try:
        rows = paginator.page(page)
    except PageNotAnInteger:
        rows = paginator.page(1)
    except EmptyPage:
        rows = paginator.page(paginator.num_pages)
    return render(request, 'table_read/table_read.html', {
        'dataset': dataset,
        'header': header,
        'rows': rows,
    })


@allowed_groups("datasets")
@login_required(login_url=settings.LOGIN_URL)
def export(request, datastore_name, dataset_name, export_format):
    response = HttpResponse(content_type='text/{}'.format(export_format))
    cdisp = 'attachment; filename="{}.{}"'.format(dataset_name, export_format)
    response['Content-Disposition'] = cdisp
    datastore = STORAGE_BACKEND.get_datastore(datastore_name)
    dataset = datastore.get_dataset(dataset_name)
    dataset.export(export_format, response)
    return response
