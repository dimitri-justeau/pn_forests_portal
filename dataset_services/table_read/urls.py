# coding: utf-8

from django.conf.urls import url

from dataset_services.table_read import views

urlpatterns = [
    url(r'^([a-z_0-9]*)/([a-z_0-9]*)/table_read/$', views.table_read),
    url(r'^([a-z_0-9]*)/([a-z_0-9]*)/export/([.a-z_1-9]*)/$', views.export),
]
