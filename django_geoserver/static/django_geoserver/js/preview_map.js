(function($, undefined) {

    // Add EPSG:32758 projection
    proj4.defs("EPSG:32758",
               "+proj=utm +zone=58 +south +datum=WGS84 +units=m +no_defs");

    var target = 'map';
    var view = new ol.View({
        projection: 'EPSG:32758',
        center: new ol.proj.transform([165.875, -21.145],
                                      'EPSG:4326',
                                      'EPSG:32758'),
        zoom: 7.5
    });
    var map = new ol.Map({
        target: target,
        layers: [
            new ol.layer.Tile({
                source: new ol.source.TileWMS({
                    url: 'http://carto.gouv.nc/arcgis/services/fond_imagerie/MapServer/WMSServer',
                    params: {
                        LAYERS: '0',
                        FORMAT: 'image/png',
                        CRS: 'EPSG:32758'
                    },
                    serverType: 'mapserver'
                })
            }),
            new ol.layer.Tile({
//                extent: [-13884991, 2870341, -7455066, 6338219],
                source: new ol.source.TileWMS({
                    url: wms_url,
                    params: {'LAYERS': layer},
                    serverType: 'geoserver'
                })
            })
        ],
        view: view,
        controls: [
            new ol.control.Zoom(),
        ]
    });
})();
