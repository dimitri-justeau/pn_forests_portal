# coding: utf-8

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.conf import settings
from geoserver.catalog import Catalog
from owslib.wms import WebMapService

from pn_forests_portal.groups import allowed_groups


SERVICE_URL = "http://localhost:8080/geoserver/rest/"
GEOSERVER_URL = settings.GEOSERVER_URL
REST_USERNAME = "rest_user"
REST_PASSWORD = "rest_user"


def _get_catalog(user):
    return Catalog(SERVICE_URL, REST_USERNAME, REST_PASSWORD)


@allowed_groups("geoserver")
@login_required(login_url=settings.LOGIN_URL)
def geoserver_index(request):
    cat = _get_catalog(request.user)
    workspaces = cat.get_workspaces()
    return render(request, 'django_geoserver/geoserver_home.html',
                  {"workspaces": [w for w in workspaces if cat.get_stores(w)]})


@allowed_groups("geoserver")
@login_required(login_url=settings.LOGIN_URL)
def workspace_index(request, workspace_name):
    workspace = _get_catalog(request.user).get_workspace(workspace_name)
    stores = _get_catalog(request.user).get_stores(workspace)
    return render(request, 'django_geoserver/geoserver_workspace.html',
                  {"workspace": workspace,
                   "stores": stores})


@allowed_groups("geoserver")
@login_required(login_url=settings.LOGIN_URL)
def store_index(request, workspace_name, store_name):
    workspace = _get_catalog(request.user).get_workspace(workspace_name)
    store = _get_catalog(request.user).get_store(store_name, workspace)
    layers = store.get_resources()
    layers_metadata = list()
    layers_links = list()
    for layer in layers:
        wms_url = "{}/{}/{}/wms".format(
            GEOSERVER_URL,
            workspace.name,
            layer.name,
        )
        wms = WebMapService(wms_url)
        layer_metadata = wms[layer.name]
        layers_metadata.append(layer_metadata)
        layers_links.append([("wms", wms_url), ])
    return render(request, 'django_geoserver/geoserver_store.html',
                  {"workspace": workspace,
                   "store": store,
                   "geoserver_url": GEOSERVER_URL,
                   "layers": zip(layers, layers_metadata, layers_links)})


@allowed_groups("geoserver")
@login_required(login_url=settings.LOGIN_URL)
def layer_preview(request, workspace_name, store_name, layer_name):
    cat = _get_catalog(request.user)
    workspace = cat.get_workspace(workspace_name)
    store = cat.get_store(store_name, workspace)
    layer = cat.get_layer(layer_name)
    wms_url = "{}/{}/{}/wms".format(
        GEOSERVER_URL,
        workspace.name,
        layer.name,
    )
    wms = WebMapService(wms_url)
    layer_metadata = wms[layer.name]
    return render(request, 'django_geoserver/geoserver_preview.html',
                  {"workspace": workspace,
                   "store": store,
                   "geoserver_url": GEOSERVER_URL,
                   "layer": layer,
                   "wms_url": wms_url,
                   "layer_metadata": layer_metadata})
