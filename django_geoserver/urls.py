# coding: utf-8

from django.conf.urls import url

from django_geoserver import views

urlpatterns = [
    url(r'^$', views.geoserver_index),
    url(r'^([a-z_0-9\w .]*)/$', views.workspace_index),
    url(r'^([a-z_0-9\w .]*)/([a-z_0-9\w .]*)$', views.store_index),
    url(r'^([a-z_0-9\w .]*)/([a-z_0-9\w .]*)/([a-z_0-9\w .]*)/preview$', views.layer_preview),
]
