# coding: utf-8

from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.views.generic import TemplateView

from django.contrib import admin

from .views import CustomSignupView, peggy_la_cochonne


urlpatterns = patterns(
    "",
    url(r"^$", TemplateView.as_view(template_name="homepage.html"), name="home"),
    url(r"^admin/", include(admin.site.urls)),
    url(r"^account/signup/$", CustomSignupView.as_view(), name="account_signup"),
    url(r"^account/", include("account.urls")),
    url(r"^rapid_inventories/", include("rapid_inventories.urls")),
    url(r"^digitizing/", include("forest_digitizing.urls")),
    url(r"^datasets/", include("datasets.urls")),
    url(r"^datasets_admin/", include("datasets_admin.urls")),
    url(r"^occurrences/", include("occurrences.urls")),
    url(r"^geoserver/", include("django_geoserver.urls"), )
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
