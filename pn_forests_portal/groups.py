# coding: utf-8

from django.http.response import Http404


def allowed_groups(*groups):
    def allowed_groups_decorator(view):
        def view_wrapper(*args, **kwargs):
            request = args[0]
            for group in groups:
                if request.user.groups.filter(name=group).exists():
                    return view(*args, **kwargs)
            raise Http404
        return view_wrapper
    return allowed_groups_decorator
