from importlib import import_module

from django.apps import AppConfig as BaseAppConfig


class AppConfig(BaseAppConfig):

    name = "pn_forests_portal"

    def ready(self):
        import_module("pn_forests_portal.receivers")
