# coding: utf-8

from django.conf import settings
from django.dispatch import receiver
from django.core.mail import send_mail

from account.signals import password_changed
from account.signals import user_sign_up_attempt, user_signed_up
from account.signals import user_login_attempt, user_logged_in

from pinax.eventlog.models import log


@receiver(user_logged_in)
def handle_user_logged_in(sender, **kwargs):
    log(
        user=kwargs.get("user"),
        action="USER_LOGGED_IN",
        extra={}
    )


@receiver(password_changed)
def handle_password_changed(sender, **kwargs):
    log(
        user=kwargs.get("user"),
        action="PASSWORD_CHANGED",
        extra={}
    )


@receiver(user_login_attempt)
def handle_user_login_attempt(sender, **kwargs):
    log(
        user=None,
        action="LOGIN_ATTEMPTED",
        extra={
            "username": kwargs.get("username"),
            "result": kwargs.get("result")
        }
    )


@receiver(user_sign_up_attempt)
def handle_user_sign_up_attempt(sender, **kwargs):
    log(
        user=None,
        action="SIGNUP_ATTEMPTED",
        extra={
            "username": kwargs.get("username"),
            "email": kwargs.get("email"),
            "result": kwargs.get("result")
        }
    )


@receiver(user_signed_up)
def handle_user_signed_up(sender, **kwargs):
    log(
        user=kwargs.get("user"),
        action="USER_SIGNED_UP",
        extra={}
    )
    user = kwargs.get("user")
    msg = "L'utilisateur {} s'est inscrit sur numberone:\n"\
        + "Nom: {}\nPrénom: {}\nEmail: {}.\nBourre-le!"
    msg = msg.format(
        user.username,
        user.last_name,
        user.first_name,
        user.email
    )
    send_mail("L'enculé, y'a un nouvel inscrit sur numberone!",
              msg, settings.EMAIL_HOST_USER, settings.ADMIN_EMAILS)
