# coding: utf-8

from django import template


register = template.Library()


@register.filter
def key_value(dict_object, key):
    return dict_object[key]
