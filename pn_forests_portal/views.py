# coding: utf-8

import account.views
from django.shortcuts import render

from .forms import CustomSignupForm
from pn_forests_portal.groups import allowed_groups


class CustomSignupView(account.views.SignupView):

    form_class = CustomSignupForm

    def after_signup(self, form):
        self.created_user.first_name = form.data['first_name']
        self.created_user.last_name = form.data['last_name']
        self.created_user.save()
        super(CustomSignupView, self).after_signup(form)


@allowed_groups("peggy_la_cochonne")
def peggy_la_cochonne(request):
    return render(request, "peggy_la_cochonne.html")
