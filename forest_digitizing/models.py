# coding: utf-8

import os
import json

from django.contrib.auth.models import User
from django.contrib.gis.db import models
from djgeojson.serializers import Serializer as GeoSerializer

from pn_forests_portal import settings
from forest_digitizing import db_routines
from forest_digitizing.topojson import geojson_to_topojson
from overwrite_storage import OverwriteStorage


class Massif(models.Model):
    """
    Mountain massif of New Caledonia.
    """

    id = models.IntegerField(primary_key=True)
    uuid = models.UUIDField()
    key_name = models.CharField(max_length=30, unique=True)
    name = models.CharField(max_length=30, unique=True)
    geom = models.PolygonField(srid=32758, blank=True, null=True)
    geom_north = models.PolygonField(srid=32758, blank=True, null=True)

    objects = models.GeoManager()

    @property
    def _assignation(self):
        if hasattr(self, 'assignation'):
            return self.assignation
        return None

    @property
    def _stats(self):
        if hasattr(self, 'stats'):
            return self.stats
        return None

    @property
    def surface(self):
        if self._stats is not None:
            return self.stats.surface
        return None

    @property
    def operator(self):
        if self._assignation is not None:
            if self.assignation.operator_id is not None:
                return self.assignation.operator
        return None

    @property
    def operator_name(self):
        if self.operator is not None:
            return self.operator.get_full_name()
        return 'Non attribué'

    @property
    def status(self):
        if self._assignation is not None:
            return self.assignation.status
        return None

    @property
    def status_display(self):
        if self.status is not None:
            return self.assignation.get_status_display()
        return 'Inconnu'

    @property
    def file_available(self):
        if self._assignation is not None:
            return self.assignation.spatialite_file not in ('', None)

    @property
    def key_name_nosep(self):
        return self.key_name.replace('_', '')

    def __str__(self):
        return self.name


class MassifStats(models.Model):
    """
    Contains some stats about a massif.
    """

    massif = models.OneToOneField(Massif, related_name='stats',
                                  primary_key=True)
    surface = models.FloatField()

    objects = models.GeoManager()

    def __str__(self):
        return "{} stats".format(self.massif.name)


class MassifAssignation(models.Model):
    """
    Massif forests digitizing assignation.
    """

    NOT_DIGITIZED = 0
    BEING_DIGITIZED = 1
    DIGITIZED = 2
    STATUS_CHOICES = (
        (NOT_DIGITIZED, 'Non digitalisé'),
        (BEING_DIGITIZED, 'En cours'),
        (DIGITIZED, 'A valider'),
    )

    massif = models.OneToOneField(Massif, related_name='assignation',
                                  primary_key=True)
    operator = models.ForeignKey(User, null=True)
    status = models.IntegerField(default=0, choices=STATUS_CHOICES)
    spatialite_file = models.FileField(upload_to='spatialite_files',
                                       storage=OverwriteStorage(),
                                       null=True)

    @property
    def massif_name(self):
        return self.massif.name

    def save(self, *args, **kwargs):
        """
        Overloading of model's save method to fill the postgis database
        after a spatialite had been uploaded.
        """
        super(MassifAssignation, self).save(*args, **kwargs)
        db_url = self.spatialite_file.url
        if not db_url:
            return
        db_path = os.path.join(settings.PACKAGE_ROOT, db_url)
        # Get the data from the spatialite
        f_table = ForestFragment3k._meta.db_table
        db_routines.update_forest3k_from_spatialite(db_path, self.massif,
                                                    f_table)
        p_table = DigitizingProblem._meta.db_table
        db_routines.update_digitizing_problems_from_spatialite(db_path,
                                                               self.massif,
                                                               p_table)
        cache_forest_fragments_3k(self.massif)


class ForestFragment3k(models.Model):
    """
    Represent a 3k digitized forest fragment, bound to a massif.
    """

    uuid = models.UUIDField()
    massif = models.ForeignKey(Massif)
    created = models.DateTimeField(null=True)
    created_by = models.ForeignKey(User, null=True,
                                   related_name='created_fragments3k')
    modified = models.DateTimeField(null=True)
    modified_by = models.ForeignKey(User, null=True,
                                    related_name='modified_fragments3k')
    comments = models.TextField(null=True)
    geom = models.MultiPolygonField(srid=32758)

    objects = models.GeoManager()

    def __str__(self):
        return "Forest fragment 3k - {}/{}".format(self.massif.name,
                                                   self.uuid)


class ForestFragment30k(models.Model):
    """
    Represent a 30k digitized forest fragment, bound to a massif.
    """

    uuid = models.UUIDField()
    massif = models.ForeignKey(Massif)
    created = models.DateTimeField(null=True)
    modified = models.DateTimeField(null=True)
    comments = models.TextField(null=True)
    geom = models.MultiPolygonField(srid=32758)

    objects = models.GeoManager()

    def __str__(self):
        return "Forest fragment 30k - {}/{}".format(self.massif.name,
                                                    self.uuid)


class DigitizingProblem(models.Model):
    """
    Digitizing problems.
    """

    uuid = models.UUIDField(null=True)
    massif = models.ForeignKey(Massif)
    location = models.PointField(srid=32758, blank=False, unique=True)
    created = models.DateTimeField(null=True)
    created_by = models.ForeignKey(User, related_name='created_problems')
    modified = models.DateTimeField(null=True)
    modified_by = models.ForeignKey(User, null=True,
                                    related_name='modified_problems')
    problem = models.CharField(max_length=255, null=True)
    comments = models.TextField(null=True)

    objects = models.GeoManager()

    @property
    def creator_username(self):
        return self.created_by.username

    @property
    def creator_full_name(self):
        return self.created_by.get_full_name()

    def __str__(self):
        return "Problem - {} - {} - {}".format(self.massif.name,
                                               self.created_by.get_full_name(),
                                               self.location)


class ProblemComment(models.Model):
    """
    Comment on a Digitizing problem.
    """

    digitizing_problem = models.ForeignKey(DigitizingProblem)
    created = models.DateTimeField(null=True)
    created_by = models.ForeignKey(User, related_name='created_comments')
    comment = models.TextField(null=True)

    objects = models.GeoManager()

    @property
    def creator_username(self):
        return self.created_by.username

    @property
    def creator_full_name(self):
        return self.created_by.get_full_name()

    def __str__(self):
        return "Comment - {} - {}".format(self.created_by.get_full_name(),
                                          self.comment)


###################
# Cache functions #
###################

def cache_forest_fragments_30k(massif):
    """
    Generate a topojson file for a given massif's forest fragments 30k,
    in order to use it as a cache in the web app.
    :param massif: The massif whose forest fragments 30k must be cached.
    """
    p = os.path.join(settings.MEDIA_ROOT,
                     'topojson_files',
                     'forest_30k',
                     "{}_30k.topojson".format(massif.key_name))
    forest_fragments_30k = ForestFragment30k.objects.filter(massif=massif)
    f_properties = ('id', )
    f_geojson = GeoSerializer().serialize(forest_fragments_30k,
                                          srid=32758,
                                          geometry_field='geom',
                                          properties=f_properties)
    with open(p, 'w') as topo:
        topo.write(json.dumps(geojson_to_topojson(f_geojson)))


def cache_forest_fragments_3k(massif):
    """
    Generate a topojson file for a given massif's forest fragments 3k,
    in order to use it as a cache in the web app.
    :param massif: The massif whose forest fragments 3k must be cached.
    """
    p = os.path.join(settings.MEDIA_ROOT,
                     'topojson_files',
                     'forest_3k',
                     "{}_3k.topojson".format(massif.key_name))
    forest_fragments_3k = ForestFragment3k.objects.filter(massif=massif)
    f_properties = ('id', )
    f_geojson = GeoSerializer().serialize(forest_fragments_3k,
                                          srid=32758,
                                          geometry_field='geom',
                                          properties=f_properties)
    with open(p, 'w') as topo:
        topo.write(json.dumps(geojson_to_topojson(f_geojson)))
