# coding: utf-8

from forest_digitizing.views.auth_views import *
from forest_digitizing.views.massifs_views import *
from forest_digitizing.views.forest_views import *
from forest_digitizing.views.user_page_views import *
