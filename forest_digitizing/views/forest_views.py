# coding: utf-8

import os

from datetime import datetime
import json
import uuid

from django.contrib.gis.geos.point import Point
from django.core.exceptions import ObjectDoesNotExist
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from djgeojson.serializers import Serializer as GeoSerializer
from django.contrib.auth.decorators import login_required
from django.conf import settings

from pn_forests_portal.groups import allowed_groups
from forest_digitizing import topojson
from forest_digitizing.models import Massif, ForestFragment3k, \
    DigitizingProblem, ProblemComment, ForestFragment30k


DATA_APPROXIMATIVE_SIZE = {
    'amoss': 213411,
    'amoa_tiwaka': 416289,
    'amoss': 213411,
    'aoupinie': 435835,
    'arago': 1106455,
    'ateu_tiaoue_neami': 864072,
    'bas_coulna_haut_coulna': 107495,
    'colnett': 592493,
    'gocoweri': 409639,
    'goro_ate': 451353,
    'goro_je': 325171,
    'grota': 357689,
    'ignambi': 601001,
    'kaapo': 262354,
    'komedo': 563735,
    'mandjelia_balade': 478328,
    'me_kanin_sphinx': 497434,
    'ouahat_ouango': 670062,
    'ouaieme_hienghene': 683944,
    'pamboa': 232225,
    'plateau_tango': 336279,
    'sources_neaoua': 350392,
    'tchingu': 546804,
    'watilu': 352365,
    'default': 450000,
}


@allowed_groups("digitizing")
@login_required(login_url=settings.LOGIN_URL)
def forets_index(request, massif_key_name=None):
    if massif_key_name is None:
        return redirect('/digitizing/')
    try:
        massif = Massif.objects.get(key_name=massif_key_name)
        properties = ('id', 'key_name', 'name', 'surface')
        m_geojson = GeoSerializer().serialize([massif], srid=32758,
                                              geometry_field='geom_north',
                                              properties=properties)
        data_size = DATA_APPROXIMATIVE_SIZE['default']
        if massif_key_name in DATA_APPROXIMATIVE_SIZE:
            data_size = DATA_APPROXIMATIVE_SIZE[massif_key_name]
        return render(request, 'forest_digitizing/forets.html',
                      {'massif': massif,
                       'massif_geojson': m_geojson,
                       'data_size': data_size})
    except ObjectDoesNotExist:
        return redirect('/digitizing/')
    except:
        raise


@allowed_groups("digitizing")
@login_required(login_url=settings.LOGIN_URL)
def forets_data(request, massif_key_name=None):
    if massif_key_name is None:
        return redirect('/digitizing/')
    try:
        massif = Massif.objects.get(key_name=massif_key_name)
        # Problems
        problems = DigitizingProblem.objects.filter(massif=massif)
        p_properties = ('id', 'creator_username', 'problem', 'comments',
                        'creator_full_name')
        problem_geojson = GeoSerializer().serialize(problems, srid=32758,
                                                    geometry_field='location',
                                                    properties=p_properties)
        # Forest
        f_properties = ('id', )
        path30k = os.path.join(settings.FOREST_30K_TOPOJSON,
                               '{}_30k.topojson'.format(massif.key_name))
        path3k = os.path.join(settings.FOREST_3K_TOPOJSON,
                              '{}_3k.topojson'.format(massif.key_name))
        # Use forest fragments 30k cached topojson if available
        print(path30k)
        if os.path.exists(path30k):
            with open(path30k, 'r') as file_30k:
                topo30k = json.loads(file_30k.read())
        else:
            frags_30k = ForestFragment30k.objects.filter(massif=massif)
            geo_30k = GeoSerializer().serialize(frags_30k,
                                                srid=32758,
                                                geometry_field='geom',
                                                properties=f_properties)
            topo30k = topojson.geojson_to_topojson(geo_30k)
        # Use forest fragments 3k cached topojson if available
        if os.path.exists(path3k):
            with open(path3k, 'r') as file_3k:
                topo3k = json.loads(file_3k.read())
        else:
            frags_3k = ForestFragment3k.objects.filter(massif=massif)
            geo_3k = GeoSerializer().serialize(frags_3k,
                                               srid=32758,
                                               geometry_field='geom',
                                               properties=f_properties)
            topo3k = topojson.geojson_to_topojson(geo_3k)
    except ObjectDoesNotExist:
        return redirect('/massifs/')
    except:
        raise
    return HttpResponse(json.dumps({
        'forest_topojson': topo3k,
        'forest30k_topojson': topo30k,
        'problem_geojson': problem_geojson,
    }))


@allowed_groups("digitizing")
@login_required(login_url=settings.LOGIN_URL)
def add_problem(request, massif_key_name=None):
    if request.method == 'POST' and massif_key_name is not None:
        user = request.user
        x = float(request.POST.get('x'))
        y = float(request.POST.get('y'))
        problem_type = request.POST.get('problem')
        comment = request.POST.get('comment')
        massif = Massif.objects.get(key_name=massif_key_name)
        pb = DigitizingProblem(uuid=uuid.uuid1(), massif=massif,
                               location=Point(x, y), created=datetime.now(),
                               created_by=user, problem=problem_type,
                               comments=comment)
        pb.save()
        return HttpResponse(json.dumps({
            'id': pb.id,
            'problem': pb.problem,
            'creator_full_name': user.get_full_name(),
            'creator_username': user.username,
            'comments': pb.comments,
        }))


@allowed_groups("digitizing")
@login_required(login_url=settings.LOGIN_URL)
def delete_problem(request):
    if request.method == 'POST':
        user = request.user
        problem_id = request.POST.get('problem_id')
        problem = DigitizingProblem.objects.get(id=problem_id)
        if problem.created_by == user:
            problem.delete()
            return HttpResponse()


@allowed_groups("digitizing")
@login_required(login_url=settings.LOGIN_URL)
def load_comments(request):
    if request.method == 'GET':
        problem_id = request.GET.get('problem_id')
        comments = ProblemComment.objects \
            .filter(digitizing_problem_id=int(problem_id)) \
            .order_by('created')
        json_comments = list()
        for c in comments:
            created = c.created.astimezone(tz=None)
            comm = {
                'id': c.id,
                'created': created.strftime('%d-%B-%Y %H:%M'),
                'creator_username': c.creator_username,
                'creator_full_name': c.creator_full_name,
                'comment': c.comment,
            }
            json_comments.append(comm)
        return HttpResponse(json.dumps(json_comments))


@allowed_groups("digitizing")
@login_required(login_url=settings.LOGIN_URL)
def add_comment(request):
    if request.method == 'POST':
        user = request.user
        problem_id = request.POST.get('problem_id')
        comment = request.POST.get('comment')
        created = datetime.now()
        problem = DigitizingProblem.objects.get(id=problem_id)
        comm = ProblemComment(digitizing_problem=problem, created=created,
                              comment=comment, created_by=user)
        comm.save()
        return HttpResponse(json.dumps({
            'id': comm.id,
            'created': comm.created.strftime('%Y-%m-%d %H:%M:%S'),
            'creator_username': comm.creator_username,
            'creator_full_name': comm.creator_full_name,
            'comment': comm.comment,
        }))
