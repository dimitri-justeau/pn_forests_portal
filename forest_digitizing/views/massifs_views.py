# coding: utf-8

import json

from django.http.response import HttpResponse
from django.shortcuts import render
from djgeojson.serializers import Serializer as GeoSerializer
from django.contrib.auth.decorators import login_required
from django.conf import settings

from forest_digitizing import topojson
from forest_digitizing.models import Massif
from pn_forests_portal.groups import allowed_groups


MASSIF_DATA_APPROXIMATIVE_SIZE = 146894


@allowed_groups("digitizing")
@login_required(login_url=settings.LOGIN_URL)
def massifs_index(request):
    massifs = Massif.objects.select_related('assignation__operator')
    return render(request, 'forest_digitizing/massifs.html',
                  {'massifs': massifs.order_by('name'),
                   'data_size': MASSIF_DATA_APPROXIMATIVE_SIZE})


@allowed_groups("digitizing")
@login_required(login_url=settings.LOGIN_URL)
def massif_data(request):
    massifs = Massif.objects \
        .select_related('assignation__operator') \
        .select_related('stats')
    properties = {
        'id': 'id',
        'key_name': 'key_name',
        'name': 'name',
        'surface': 'surface',
        'operator_name': 'operator',
        'status': 'status',
        'status_display': 'status_display',
        'file_available': 'file_available',
    }
    m_geojson = GeoSerializer().serialize(massifs, srid=32758,
                                          geometry_field='geom_north',
                                          properties=properties)
    topo = topojson.geojson_to_topojson(m_geojson)
    return HttpResponse(json.dumps(topo))
