# coding: utf-8

from django.contrib.auth import logout
from django.contrib.auth.views import login, password_change
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.conf import settings


@login_required(login_url=settings.LOGIN_URL)
def logout_view(request):
    logout(request)
    return redirect('/digitizing/')


def login_view(request):
    return login(request)


@login_required(login_url=settings.LOGIN_URL)
def password_change_view(request, return_url=None):
    if return_url is None:
        return_url = '/digitizing/'
    return password_change(request, post_change_redirect=return_url,
                           extra_context={'cancel_url': return_url, })
