# coding: utf-8

import os

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from sendfile import sendfile
from django.conf import settings
from forest_digitizing import db_routines

from forest_digitizing.models import Massif


@login_required(login_url=settings.LOGIN_URL)
def user_page_index(request):
    user = request.user
    massifs = Massif.objects.filter(assignation__operator=user) \
        .select_related('assignation')
    return render(request, 'forest_digitizing/user_page.html',
                  {'massifs': massifs})


@login_required(login_url=settings.LOGIN_URL)
def download_spatialite(request, key_name):
    massif = Massif.objects.get(key_name=key_name)
    file_url = massif.assignation.spatialite_file.url
    file_path = os.path.join(settings.MEDIA_ROOT, file_url)
    # Update problems
    db_routines.replace_spatialite_digitizing_problems(key_name, file_path)
    # Send the file to be downloaded
    file_name = '{}.sqlite'.format(massif.key_name_nosep)
    return sendfile(request, file_path, attachment=True,
                    attachment_filename=file_name)
