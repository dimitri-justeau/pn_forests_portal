# coding: utf-8

from django.contrib.gis import admin
from forest_digitizing.models import Massif, MassifAssignation, MassifStats, \
    DigitizingProblem, ProblemComment


class MassifAdmin(admin.GeoModelAdmin):
    fields = ('id', 'name', 'geom_north')
    list_display = ('id', 'key_name', 'name')
    readonly_fields = ('id', 'key_name', 'name')
    modifiable = False


class MassifAssignationAdmin(admin.ModelAdmin):
    fields = ('massif', 'operator', 'status', 'spatialite_file')
    list_display = ('massif_name', 'operator', 'status', 'spatialite_file')


class MassifStatsAdmin(admin.ModelAdmin):
    fields = ('massif', 'surface')
    list_display = ('massif', 'surface')


class DigitizingProblemAdmin(admin.ModelAdmin):
    list_display = ('massif', 'created_by', 'problem', 'comments')


class ProblemCommentAdmin(admin.ModelAdmin):
    list_display = ('created_by', 'digitizing_problem', 'comment')


admin.site.register(Massif, MassifAdmin)
admin.site.register(MassifAssignation, MassifAssignationAdmin)
admin.site.register(MassifStats, MassifStatsAdmin)
admin.site.register(DigitizingProblem, DigitizingProblemAdmin)
admin.site.register(ProblemComment, ProblemCommentAdmin)
