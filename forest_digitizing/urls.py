# coding: utf-8

from django.conf.urls import url

from forest_digitizing import views


urlpatterns = [
    # Massifs page
    url(r'^$', views.massifs_index, name='massifs_index'),
    url(r'^data$', views.massif_data, name='massifs_data'),
    url(r'^$', views.forets_index, name='forets_index'),
    # Forest view page
    url(r'^forest/([a-z_]*)/$', views.forets_index, name='forets_index'),
    url(r'^forest/([a-z_]*)/data$', views.forets_data, name='forets_data'),
    url(r'^forest/([a-z_]*)/add_problem$',
        views.add_problem,
        name='add_comment'),
    url(r'^forest/[a-z_]*/delete_problem$',
        views.delete_problem,
        name='delete_problem'),
    url(r'^forest/[a-z_]*/load_comments',
        views.load_comments,
        name="load_comments"),
    url(r'^forest/[a-z_]*/add_comment',
        views.add_comment,
        name='add_comment'),
]
