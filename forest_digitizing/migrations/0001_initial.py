# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import django.contrib.gis.db.models.fields
import overwrite_storage


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='DigitizingProblem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('uuid', models.UUIDField(null=True)),
                ('location', django.contrib.gis.db.models.fields.PointField(srid=32758, unique=True)),
                ('created', models.DateTimeField(null=True)),
                ('modified', models.DateTimeField(null=True)),
                ('problem', models.CharField(max_length=255, null=True)),
                ('comments', models.TextField(null=True)),
                ('created_by', models.ForeignKey(related_name='created_problems', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ForestFragment30k',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('uuid', models.UUIDField()),
                ('created', models.DateTimeField(null=True)),
                ('modified', models.DateTimeField(null=True)),
                ('comments', models.TextField(null=True)),
                ('geom', django.contrib.gis.db.models.fields.MultiPolygonField(srid=32758)),
            ],
        ),
        migrations.CreateModel(
            name='ForestFragment3k',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('uuid', models.UUIDField()),
                ('created', models.DateTimeField(null=True)),
                ('modified', models.DateTimeField(null=True)),
                ('comments', models.TextField(null=True)),
                ('geom', django.contrib.gis.db.models.fields.MultiPolygonField(srid=32758)),
                ('created_by', models.ForeignKey(null=True, related_name='created_fragments3k', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Massif',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('uuid', models.UUIDField()),
                ('key_name', models.CharField(max_length=30, unique=True)),
                ('name', models.CharField(max_length=30, unique=True)),
                ('geom', django.contrib.gis.db.models.fields.PolygonField(blank=True, null=True, srid=32758)),
                ('geom_north', django.contrib.gis.db.models.fields.PolygonField(blank=True, null=True, srid=32758)),
            ],
        ),
        migrations.CreateModel(
            name='ProblemComment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('created', models.DateTimeField(null=True)),
                ('comment', models.TextField(null=True)),
                ('created_by', models.ForeignKey(related_name='created_comments', to=settings.AUTH_USER_MODEL)),
                ('digitizing_problem', models.ForeignKey(to='forest_digitizing.DigitizingProblem')),
            ],
        ),
        migrations.CreateModel(
            name='MassifAssignation',
            fields=[
                ('massif', models.OneToOneField(to='forest_digitizing.Massif', serialize=False, related_name='assignation', primary_key=True)),
                ('status', models.IntegerField(default=0, choices=[(0, 'Non digitalisé'), (1, 'En cours'), (2, 'A valider')])),
                ('spatialite_file', models.FileField(null=True, storage=overwrite_storage.OverwriteStorage(), upload_to='spatialite_files')),
                ('operator', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='MassifStats',
            fields=[
                ('massif', models.OneToOneField(to='forest_digitizing.Massif', serialize=False, related_name='stats', primary_key=True)),
                ('surface', models.FloatField()),
            ],
        ),
        migrations.AddField(
            model_name='forestfragment3k',
            name='massif',
            field=models.ForeignKey(to='forest_digitizing.Massif'),
        ),
        migrations.AddField(
            model_name='forestfragment3k',
            name='modified_by',
            field=models.ForeignKey(null=True, related_name='modified_fragments3k', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='forestfragment30k',
            name='massif',
            field=models.ForeignKey(to='forest_digitizing.Massif'),
        ),
        migrations.AddField(
            model_name='digitizingproblem',
            name='massif',
            field=models.ForeignKey(to='forest_digitizing.Massif'),
        ),
        migrations.AddField(
            model_name='digitizingproblem',
            name='modified_by',
            field=models.ForeignKey(null=True, related_name='modified_problems', to=settings.AUTH_USER_MODEL),
        ),
    ]
