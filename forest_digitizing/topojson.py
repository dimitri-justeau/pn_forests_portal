# coding: utf-8

import os
import json

import execjs


def geojson_to_topojson(geojson, **kwargs):
    """
    Convert geojson to topojson using Node.js topojson api.
    !IMPORTANT!: Require the topojson module to be installed from npm, and the
                 NODE_PATH environment variable to be set.
    :param geojson: The geojson to convert.
    :param kwargs: The options to pass to the topojson api.
    :type geojson: str
    :return: A topojson representing the same data as the given geojson.
    """
    if not os.environ.get('NODE_PATH'):
        os.environ['NODE_PATH'] = '/usr/local/lib/node_modules'
    runtime = execjs.get('Node')
    context = runtime.compile("""
        topojson = require('topojson');
        convert = function(object, options) {
            var objects = {};
            if (object.type === 'Topology') {
                for (var key in object.objects) {
                    object[key] = topojson.feature(object,
                                                   object.objects[key]);
                }
            } else {
                objects['f'] = object;
            }
            return topojson.topology(
                objects,
                {
                    "property-transform": function(object) {
                        return object.properties;
                    }
                }
            );
        };""")
    return context.call('convert', json.loads(geojson))
