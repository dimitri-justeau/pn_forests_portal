/**
* Problems management for forest visualization app.
*/

goog.require('olext.overlay.Popup');

goog.require('forest.comments');

goog.provide('forest.problems');


var problem_desc_element = document.getElementById('problem-desc');
forest.problems.problem_source = null;
forest.problems.problem_select = null;

forest.problems.popup = new olext.overlay.Popup({
    'positioning': 'bottom-center',
    'autoPan': true,
    'autoPanAnimation': {
        'duration': 250
    },
    'hide_callback': function() {
        forest.problems.setCurrentProblem(null);
        forest.problems.problem_select.getFeatures().clear();
    }
});

forest.problems.setCurrentProblem = function(problem) {
    forest.problems.showProblemPopup(problem);
    forest.problems.updateSelectedProblemDescription(problem);
    if (problem) {
        forest.comments.loadComments(problem);
        forest.comments.showAddComment();
    } else {
        forest.comments.hideAddComment();
        forest.comments.setCommentsList([]);
    }
};

forest.problems.showProblemPopup = function(problem) {
    if (problem) {
        var full_name = problem.get('creator_full_name')
        var title = '<b>' + full_name + '</b>';
        var coords = problem.getGeometry().getCoordinates();
        var pb_type = problem.get('problem');
        var pb = '';
        var comms = problem.get('comments');
        if (pb_type) {
            pb = '<em>' + pb_type + ' </em>';
        }
        if (comms) {
            pb = pb + ': ' + comms;
        }
        forest.problems.popup.show(coords, title, pb);
    } else {
        forest.problems.popup.hide(function(){});
    }
};

forest.problems.updateSelectedProblemDescription = function(problem) {
    if (problem) {
        // Update description div
        var full_name = problem.get('creator_full_name');
        var pb_username = problem.get('creator_username');
        var pb_type = problem.get('problem');
        var pb = '';
        if (pb_type) {
            pb = '<em>' + problem.get('problem') + ': </em>';
        }
        var comment = problem.get('comments');
        var s = ['<h3>', full_name, '</h3>', pb, '<div>', comment,
                 '</div>', '<div id="pb_controls">'];
        var username = document.getElementById('username').innerHTML;
        console.log(username);
        if (username == pb_username) {
            s.push('<button id="delete_pb_button">Supprimer</button>');
        }
        s.push('</div>');
        problem_desc_element.innerHTML = s.join('');
        // Add event listeners to buttons
        if (username == pb_username) {
            var del_button = document.getElementById("delete_pb_button");
            del_button.addEventListener('click', function() {
                forest.problems.deleteProblem(problem);
            });
        }
    } else {
        problem_desc_element.innerHTML = '';
    }
};

forest.problems.deleteProblem = function(problem) {
    var csrftoken = $.cookie('csrftoken');
    $.ajaxSetup({
        'beforeSend': function(xhr, settings) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    });
    $.ajax({
        'url': 'delete_problem',
        'type': 'POST',
        'data': {
            'problem_id': problem.get('id')
        },
        'success': function(response) {
            forest.problems.problem_source.removeFeature(problem);
            forest.problems.setCurrentProblem(null);
            forest.problems.problem_select.getFeatures().clear();
        },
        'error': function(response) {
            msg = ["Erreur: Impossible de supprimer le problème.\n",
                   "Veuillez contacter l'administrateur du site si ",
                   "l'erreur persiste."];
            alert(msg.join(''));
        }
    });
};
