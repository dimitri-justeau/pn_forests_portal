/**
* Comments management for forest visualization app.
*/

goog.provide('forest.comments');


var comments_list = document.getElementById('problem-comments');
var add_comment_element = document.getElementById('add_comment_content');
var comment_text_area = document.getElementById('comment_textarea');
var comment_button = document.getElementById('comment_button');
var username = document.getElementById('username').innerHTML;
var current_problem = null;
var empty_html = 'Pas de commentaires';

forest.comments.showAddComment = function() {
    comment_text_area.value = '';
    $('#add_comment_content').show();
};

forest.comments.hideAddComment = function() {
    $('#add_comment_content').hide();
};

forest.comments.addComment = function() {
    var csrftoken = $.cookie('csrftoken');
    $.ajaxSetup({
        'beforeSend': function(xhr, settings) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    });
    $.ajax({
        'url': 'add_comment',
        'type': 'POST',
        'data': {
            'problem_id': current_problem.get('id'),
            'username': username,
            'comment': comment_text_area.value
        },
        'success': function(response) {
            forest.comments.appendComment(JSON.parse(response));
            comment_text_area.value = '';
        },
        'error': function(response) {
            msg = ["Erreur: Impossible d'ajouter le commentaire.\n",
                   "Veuillez contacter l'administrateur du site si ",
                   "l'erreur persiste."];
            alert(msg.join(''));
        }
    });
};

forest.comments.loadComments = function(selected_problem) {
    current_problem = selected_problem;
    comments_list.innerHTML = 'Chargement des commentaires...';
    $.ajax({
        'url': 'load_comments',
        'type': 'GET',
        'data': {
            'problem_id': selected_problem.get('id')
        },
        'success': function(result) {
            comments = JSON.parse(result);
            if (comments.length == 0) {
                comments_list.innerHTML = empty_html;
            } else {
                forest.comments.setCommentsList(comments);
            }
        },
        'error': function(response) {
            msg = ["Erreur: Impossible de charger.\n",
                   "Veuillez contacter l'administrateur du site si ",
                   "l'erreur persiste."];
            alert(msg.join(''));
        }
    });
};

forest.comments.appendComment = function(comment) {
    s = ['<em>', comment['created'], ', ', comment['creator_full_name'],
         ':</em><br>', comment['comment']]
    var div = document.createElement('div');
    div.innerHTML = s.join('');
    if (comments_list.innerHTML === empty_html) {
        comments_list.innerHTML = '';
    }
    comments_list.appendChild(div);
};

forest.comments.setCommentsList = function(comments) {
    comments_list.innerHTML = '';
    comments.forEach(forest.comments.appendComment);
};

comment_button.addEventListener('click', forest.comments.addComment);
