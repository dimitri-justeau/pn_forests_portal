goog.require('olext.control.ProgressBar');
goog.require('olext.control.LayersVisibility');

goog.require('forest.styles');
goog.require('forest.map');
goog.require('forest.sources');
goog.require('forest.layers');
goog.require('forest.comments');
goog.require('forest.problems');

var map = forest.map;
var view = map.getView();

map.addOverlay(forest.problems.popup);

function fitMassif() {
    view.fitExtent(forest.sources.massif.getExtent(), map.getSize());
};

function sizeContent() {
    var newHeight = $("#content").height() * 0.85;
    $(".column").css("height", newHeight + "px");
    map.updateSize();
};

$(document).ready(function() {
    // Load data
    var progress_bar = new olext.control.ProgressBar();
    progress_bar.setLabel("0%");
    map.addControl(progress_bar);
    $.ajax({
        'xhr': function() {
            var xhr = new window.XMLHttpRequest();
            xhr.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                    var progr = (evt.loaded / evt.total * 100).toFixed(1);
                    progress_bar.setValue(progr);
                    progress_bar.setLabel(progr + "%");
                } else {
                    var progr = (evt.loaded / data_size * 100).toFixed(1);
                    progress_bar.setValue(progr);
                    progress_bar.setLabel(progr + "%");
                }
            }, false);
            return xhr;
        },
        'type': 'GET',
        'url': 'data',
        'success': function(result) {
            var data = JSON.parse(result);
            var forest_topojson = data['forest_topojson'];
            var forest30k_topojson = data['forest30k_topojson'];
            var problem_geojson = data['problem_geojson'];
            forest.layers.initForest3kLayer(forest_topojson);
            forest.layers.initForest30kLayer(forest30k_topojson);
            addProblemsLayer(problem_geojson);
            var show_hide_control = new olext.control.LayersVisibility({
                'layers': [
                    forest.layers.massif,
                    forest.layers.forest3k,
                    forest.layers.forest30k,
                    forest.layers.problem
                ],
                'labels': [
                    'Massif:',
                    'Forêt (3k):',
                    'Forêt (30k):',
                    'Problèmes:'
                ],
                'initial_states': [true, true, false, true]
            });
            map.addControl(show_hide_control);
            map.removeControl(progress_bar);
            forest.problems.setCurrentProblem(null);
        }
    });

    function addProblemsLayer(problem_geojson) {
        forest.layers.initProblemLayer(problem_geojson);

    };

    $(window).resize(sizeContent);
    sizeContent();
    forest.layers.initMassifLayer(massif_geojson);
    fitMassif();
    forest.comments.hideAddComment();
});
