# coding: utf-8

from django import template
from occurrences.models import Occurrence

register = template.Library()

dataset = Occurrence.dataset()


@register.filter
def replace_location_column(row, idx):
    if idx == 2:
        return "<a href='map_view/{}/'>localisation</a>".format(row[0])
    else:
        return row[idx]
