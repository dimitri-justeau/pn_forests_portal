# coding: utf-8

from django.conf.urls import url

from occurrences import views


urlpatterns = [
    url(r'^$', views.occurrences_index),
    url(r'^export/([.a-z_0-9]*)/$', views.export),
    url(r'^map_view/([.a-z_0-9]*)/$', views.occurrence_map_view),
]
