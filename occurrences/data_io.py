# coding: utf-8

from datetime import date
import csv

from occurrences.models import Occurrence


def import_occurrences_from_csv(csv_file, header=True):
    """
    Import occurrences from csv data.
    :param data: The csv data, stored in a str.
    """
    with open(csv_file, "r") as f:
        reader = csv.reader(f)
        for row in reader:
            if header:
                header = False
                continue
            date_str = row[1]
            l = row[2]
            ll = l[6:]
            ll = ll[:len(ll)-1]
            ll = ll.split(' ')
            row[2] = "POINT({} {})".format(ll[1], ll[0])
            d = None
            identifier = row[0]
            if len(identifier) == 0:
                identifier = None
            if len(date_str) > 0:
                sd = date_str.split("/")
                if len(sd) == 3:
                    d = date(*[int(i) for i in sd])
                elif len(sd) == 2:
                    d = date(int(sd[0]), int(sd[1]), 1)
                else:
                    d = date(int(sd[0]), 1, 1)
            occurrence = Occurrence(
                identifier=identifier,
                date=d,
                location=row[2],
                taxon=row[3],
                collector="{" + row[4] + "}",
            )
            occurrence.save()
