# coding: utf-8

from django.contrib.gis.db import models
from django.contrib.postgres import fields

from datasets.settings import STORAGE_BACKEND


class Occurrence(models.Model):
    """
    Model representing a plant occurrence, in it's simplest form, i.e.
    a unique identifier, a geographic location, a taxon (if identified),
    and a collector's name.
    """

    identifier = models.CharField(unique=True, max_length=20, null=True,
                                  blank=True)
    date = models.DateField(null=True, blank=True)
    location = models.PointField(srid=4326)
    taxon = models.CharField(max_length=300)
    collector = fields.ArrayField(models.CharField(max_length=300))

    @classmethod
    def dataset(cls):
        datastore = STORAGE_BACKEND.get_datastore('pn_forests_portal')
        return datastore.get_dataset(cls._meta.db_table)
