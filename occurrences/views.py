# coding: utf-8

from django.http.response import HttpResponse
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings

from pn_forests_portal.groups import allowed_groups
from occurrences.models import Occurrence


@allowed_groups("occurrences")
@login_required(login_url=settings.LOGIN_URL)
def occurrences_index(request):
    dataset = Occurrence.dataset()
    header = dataset.get_header_names(exclude=['id'])
    occurrence_list = dataset.get_rows(exclude=['id'])
    paginator = Paginator(occurrence_list, 50)
    page = request.GET.get('page')
    try:
        occurrences = paginator.page(page)
    except PageNotAnInteger:
        occurrences = paginator.page(1)
    except EmptyPage:
        occurrences = paginator.page(paginator.num_pages)
    services = [('export', 'Export des données', ['csv', 'geojson'])]
    return render(request, 'occurrences/occurrences_index.html', {
        'dataset': dataset,
        'header': header,
        'services': services,
        'rows': occurrences,
        'replace_columns': [2, ],
    })


@allowed_groups("occurrences")
@login_required(login_url=settings.LOGIN_URL)
def export(request, export_format):
    dataset = Occurrence.dataset()
    response = HttpResponse(content_type='text/{}'.format(export_format))
    cdisp = 'attachment; filename="{}.{}"'.format(dataset.name, export_format)
    response['Content-Disposition'] = cdisp
    dataset.export(export_format, response)
    return response


@allowed_groups("occurrences")
@login_required(login_url=settings.LOGIN_URL)
def occurrence_map_view(request, occurrence_identifier):
    dataset = Occurrence.dataset()
    filters = {'identifier': occurrence_identifier, }
    row = dataset.get_rows(filters=filters)
    geojson_data = dataset.rows_to_geojson(row)
    return render(request, 'occurrences/occurrence_map_view.html', {
        'identifier': occurrence_identifier,
        'geojson_data': dataset.rows_to_geojson(row),
    })
