# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.postgres.fields
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Occurrence',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('identifier', models.CharField(max_length=20, blank=True, null=True, unique=True)),
                ('date', models.DateField(blank=True, null=True)),
                ('location', django.contrib.gis.db.models.fields.PointField(srid=4326)),
                ('taxon', models.CharField(max_length=300)),
                ('collector', django.contrib.postgres.fields.ArrayField(size=None, base_field=models.CharField(max_length=300))),
            ],
        ),
    ]
